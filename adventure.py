import pygame
from pygame.locals import *

pygame.init()
BREITE = 800
HOEHE = 600

bilder = []
for i in range(9):
    bild = pygame.image.load(f"bilder/bild{i}.png")
    bild = pygame.transform.scale(bild, (BREITE, HOEHE))
    bilder.append(bild)
screen = pygame.display.set_mode((BREITE,HOEHE))
pygame.display.set_caption("Mein Adventure")

class Spieler:
    def __init__(self) -> None:
        pass

class Raum:
    def __init__(self, image, ausgaenge) -> None:
        self.ausgaenge = ausgaenge # Tupel mit nord, ost, sue, west, hoch, runter
        self.image = image

raeume = []
raeume.append(Raum(bilder[0], (7, 1, -1, -1, -1, -1)))
raeume.append(Raum(bilder[1], (8, 2, -1, 0, -1, -1)))
raeume.append(Raum(bilder[2], (3, -1, -1, 1, -1, -1)))
raeume.append(Raum(bilder[3], (4, -1, 2, 8, -1, -1)))
raeume.append(Raum(bilder[4], (-1, -1, 3, 5, -1, -1)))
raeume.append(Raum(bilder[5], (-1, 4, 8, 6, -1, -1)))
raeume.append(Raum(bilder[6], (-1, 5, 7, -1, -1, -1)))
raeume.append(Raum(bilder[7], (6, 8, 0, -1, -1, -1)))
raeume.append(Raum(bilder[8], (5, 3, 1, 7, -1, -1)))

def move(raum, direction):
    global raeume
    if raum.ausgaenge[direction] >= 0:
        return raeume[raum.ausgaenge[direction]]
    else:
        return raum

ort = raeume[0]

running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        if event.type == KEYDOWN:
            if event.key == K_UP:
                ort = move(ort, 0)
            if event.key == K_RIGHT:
                ort = move(ort, 1)
            if event.key == K_DOWN:
                ort = move(ort, 2)
            if event.key == K_LEFT:
                ort = move(ort, 3)

    screen.blit(ort.image, (0,0))
    pygame.display.update()